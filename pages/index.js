import React from "react";

import Layout from '../src/components/Layout/Layout';
import IndexPage from "../src/pages/Index/indexPage";

export default function Home() {
  return (
    <Layout>
      <IndexPage/>
    </Layout>
  )
}
