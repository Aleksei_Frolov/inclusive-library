import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  render() {
    return (
      <Html lang={'ru'}>
        <Head>
          <Head>
            <title>Инклюзивный Музей</title>

            <link type="image/x-icon"
                  rel="icon" href="/favicon.ico" />
            <link rel="canonical" href="https://inclusive-museum.vercel.app/" />
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=1" />
            <meta name="description" content="description Инклюзивный Музей" />
            <meta name="keywords" content="description Инклюзивный Музей" />
          </Head>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
