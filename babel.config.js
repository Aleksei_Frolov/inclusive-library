module.exports = {
  presets: [
    [
      'next/babel',
      {
        'preset-env': {
          'targets': {
            'browsers': ['> 1%', 'ie 11', 'last 4 versions', 'Firefox ESR', 'not ie < 10'],
          },
        },
      },
    ],
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    'babel-plugin-inline-react-svg',
  ],
};
