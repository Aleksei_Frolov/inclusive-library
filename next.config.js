const withPlugins = require('next-compose-plugins');
const withCSS = require("@zeit/next-css");
const withSass = require("@zeit/next-sass");
const withImages = require('next-images')

const nextConfig = {
  webpack: config => {
    return config;
  }
};

module.exports = withPlugins([
  withImages,
  withCSS,
  withSass,
], nextConfig);
