import React from "react";

import './index.scss'

export default function Login({onCloseLogin}) {
  return (
    <section className="Login">
      <div className="Login__overlay" onClick={onCloseLogin}/>
      <div  className="Login__content-wrapper">
        <button id={'close-button'} type={"button"} aria-label="Закрыть" className={"Login__close-btn"} onClick={onCloseLogin}>X</button>
        <form id={'login'} className="Login__content" aria-label="Форма входа в личный кабинет">
          <h2>Вход</h2>

          <div className={"Login__control"}>
            <label className={'Login__control-label'} htmlFor={"profile-name"}>Имя пользователя</label>
            <input required aria-required className={'Login__control-input'} id={'profile-name'} type="text"/>
          </div>

          <div className={"Login__control"}>
            <label className={'Login__control-label'} htmlFor={"profile-password"}>Пароль</label>
            <input required aria-required className={'Login__control-input'} id={"profile-password"} type="password"/>
          </div>

          <div className={"Login__action"}>
            <button type={"submit"} className={"Login__enter-btn"}>Вход</button>
          </div>
        </form>
      </div>
    </section>
  )
}
