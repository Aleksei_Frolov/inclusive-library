import React from "react";

import Museum from "./Museum/Museum";
import Poster from "./Poster/Poster";
import Events from "./Events/Events";
import Translations from "./Translations/Translations";
import Live from "./Live/Live";

import "./index.scss";

export default function Main() {
  return (
    <main className="Main" id="main">
      <nav>
        <h2 className="visibility-hidden">Главное меню</h2>
        <div className="Main__menu" role="menubar">
          <a className="Main__menu-item" href="#events" role="menuitem">Выставки и события</a>
          <a className="Main__menu-item" href="#museum" role="menuitem">Музей</a>
          <a className="Main__menu-item" href="#translations" role="menuitem">Трансляции</a>
          <a className="Main__menu-item" href="#visitors" role="menuitem">Посетителям</a>
        </div>
      </nav>

      <div className="Main__content">
        <Poster />
        <Events />
        <Museum />
        <Translations />
        <Live />
      </div>
    </main>
  )
}
