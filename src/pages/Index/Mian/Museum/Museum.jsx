import React, { useState } from "react"
import cn from 'classnames';
import museumList from "./museumList";

import './index.scss';

const Mode = {
  BUILDINGS: 'buildings',
  HISTORY: 'history',
};

const events = [
  {
    id: 'museum-1',
    img: museumList.img1,
    title: "МЕМОРИАЛЬНАЯ КВАРТИРА С.Т. РИХТЕРА",
    address: "ул. Большая Бронная, 2/6, 16 этаж, кв. 58",
  },
  {
    id: 'museum-2',
    img: museumList.img2,
    title: "ЦЭВ «МУСЕЙОН",
    address: "Колымажный пер., 6/2, 3 (вход с Малого Знаменского переулка)",
  },
  {
    id: 'museum-3',
    img: museumList.img3,
    title: "УСАДЬБА ЛОПУХИНЫХ",
    address: "Малый Знаменский пер., 3/5с4",
  },
  {
    id: 'museum-4',
    img: museumList.img4,
    title: "УЧЕБНЫЙ МУЗЕЙ",
    address: "ул. Чаянова, 15",
  },
  {
    id: 'museum-5',
    img: museumList.img5,
    title: "ОТДЕЛ ЛИЧНЫХ КОЛЛЕКЦИЙ",
    address: "ул. Волхонка, 10",
  },
  {
    id: 'museum-6',
    img: museumList.img6,
    title: "ГАЛЕРЕЯ",
    address: "ул. Волхонка, 14",
  },
];

export default function Events() {
    const [mode, setMode] = useState(Mode.BUILDINGS);
  const handleClickBuildings = () => setMode(Mode.BUILDINGS);
  const handleClickHistory = () => setMode(Mode.HISTORY);

  return (
    <section id="museum" className="Museum">
      <h2>Музей</h2>
      <div className="Museums__nav">
        <button className={cn("Museum__nav-item Button-default", {active: mode === Mode.BUILDINGS})}
                type="button"
                onClick={handleClickBuildings}>Здания
        </button>
        <button className={cn("Museum__nav-item Button-default", {active: mode === Mode.HISTORY})}
                type="button"
                onClick={handleClickHistory}>История
        </button>
      </div>

      {mode === Mode.BUILDINGS && (
        <div className="Museum__buildings">
          <div className="Museum__items">
            {
              events.map(({id, img, address, title}) => (
                <div key={id} className="Museum__item">
                  <div role="img" aria-label={`изображение схемы ${title}`}
                       className="Museum__item-image"
                       style={{ backgroundImage: `url(${img})` }}
                  />
                  <div className="Museum__item-title">
                    {title}
                  </div>
                  <div className="Museum__item-address">
                    {address}
                  </div>
                  <span className="visibility-hidden" id={id}>
                    Подробнее о {title}
                  </span>
                  <a className="Museum__item-about-link" href="/#about" aria-labelledby={id}>Подробнее</a>
                </div>
              ))
            }
          </div>
        </div>
      )}

      {mode === Mode.HISTORY && (
        <div className="Museums__history">
          <p>
            Государственный музей изобразительных искусств имени А.С. Пушкина – одно из крупнейших в России
            художественных
            собраний зарубежного искусства с древнейших времен до наших дней.
          </p>
          <p>
            В современной экспозиции представлены обширная учебная коллекция тонированных гипсовых слепков с
            произведений
            Античности, Средних веков и эпохи Возрождения, а также собрание подлинных произведений живописи, скульптуры,
            графики и декоративно-прикладного искусства.
          </p>
          <p>
            В залах первого этажа Главного здания представлены: произведения искусства Древнего Египта, Античности,
            собрание европейской живописи VIII–XVIII веков; два зала – Итальянский и Греческий дворики – занимают
            слепки.
            На втором этаже Главного здания в залах размещены слепки произведений искусства Древней Греции, Рима,
            Средних
            веков и Возрождения.
          </p>
          <p>
            В начале 2017 года музей определил новое направление своей деятельности – Пушкинский XXI, – ориентированное
            на
            современное российское и западное искусство во всех аспектах – выставочном, образовательном и
            исследовательском. Таким образом, ГМИИ им. А.С. Пушкина представляет собой открытую, интерактивную сцену,
            демонстрирующую развитие мировой культуры с древности до наших дней, живое, знакомое с детства пространство
            для получения знаний и общения.
          </p>
        </div>
      )}
    </section>
  )
}
