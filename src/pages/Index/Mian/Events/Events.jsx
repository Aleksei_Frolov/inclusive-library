import React, { useState } from "react";
import cn from 'classnames';

import image1 from "../../../../assets/images/image1.svg";
import image2 from "../../../../assets/images/image2.svg";
import image3 from "../../../../assets/images/image3.svg";

import './index.scss'

export default function Translations() {
  const [filter, setFilter] = useState(0);
  const [loading, setLoading] = useState(false);
  const events = [
    {
      id: 'ticked-1',
      img: image1,
      title: "Святослав Рихтер в кругу друзей. Москва — Коктебель",
      date: "Выставка до 20 ноября",
      description: "Святослав Рихтер Текст о музее текст Текст о музее текст Текст о музее текст Текст о музее текст"
    },
    {
      id: 'ticked-2',
      img: image2,
      title: "Тату",
      date: "Выставка до 27 сентября",
      description: "Тату Текст о музее текст Текст о музее текст Текст о музее текст Текст о музее текст"
    },
    {
      id: 'ticked-3',
      img: image3,
      title: "От Дюрера до Матисса. Избранные рисунки из собрания ГМИИ им. А.С. Пушкина",
      date: "Выставка до 1 ноября",
      description: "От Дюрера до Матисса Текст о музее текст Текст о музее текст Текст о музее текст Текст о музее текст"
    }
  ]

  const filteredEvents = {
    '0': events,
    '1': [events[0], events[1]],
    '2': [events[1], events[2]],
  };

  const handleClickFilter = (filterValue) => {
    setFilter(filterValue);
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 1500)
  }

  const eventList = filteredEvents[filter];

  return (
    <section id="events" className="Events">
      <h1>Выставки и события</h1>
      <nav className="Events__nav">
        <button className={cn("Events__nav-item Button-default", { active: filter === 0})} type="button" onClick={() =>handleClickFilter(0)}>Все</button>
        <button className={cn("Events__nav-item Button-default", { active: filter === 1})} type="button" onClick={() =>handleClickFilter(1)}>Сегодня</button>
        <button className={cn("Events__nav-item Button-default", { active: filter === 2})} type="button" onClick={() =>handleClickFilter(2)}>Завтра</button>
      </nav>

      <div className={cn("Events__items", { loading })} aria-busy={loading? 'true': 'false'}>
        { loading && <div className="loader" aria-live="polite" aria-label="Идет фильтрация данных">
          <div className="loader-text">Загрузка...</div>
        </div>}
        {
          eventList.map(({ id, img: Image, date, description, title}) => (
            <div key={id} className="Events__item">
              <div className="Events__item-image">
                <Image preserveAspectRatio="xMidYMid slice" />
              </div>
              <div className="Events__item-title">
                { title }
              </div>
              <div className="Events__item-date">
                { date }
              </div>
              <div className="Events__item-description">
                { description }
              </div>
              <span className="visibility-hidden" id={id}>
                Купить билет "{title}"
              </span>
              <a className="Events__item-buy-link" href="/#buy" aria-labelledby={id}>Купить билет</a>
            </div>
          ))
        }
      </div>
    </section>
  )
}
