import React, {useState} from "react";

import PrevArrow from '../../../../assets/prev-arroe.svg';
import NextArrow from '../../../../assets/next-arroe.svg';
import images from './images/imageList';

import './index.scss'

export default function Poster() {
  const [currentImage, setCurrentImage] = useState(0);
  const imageList = [
    {src: images.image1, title: "Выставка: от Дюрера до Матиса"},
    {src: images.image2, title: "Событие: Ночь музеев. Я иду в музей"},
    {src: images.image3, title: "Событие: Ночь музеев, в Томске"}
  ];
  const imageCount = imageList.length;

  const handleClickPrev = () => {
    setCurrentImage(currentImage < 1 ? imageCount - 1 : currentImage - 1);
  }
  const handleClickNext = () => {
    setCurrentImage(currentImage >= imageCount - 1 ? 0 : currentImage + 1);
  }

  return (
    <section id="poster" className="Poster">
      <div className="Poster__carousel">
        <span className="visibility-hidden" area-label="События и выставки"/>
        <button className="Poster__carousel-nav Poster__carousel-prev" area-label="Предыдущее событие" type="button"
                onClick={handleClickPrev}>
          <span className="visibility-hidden" area-label="Предыдущее событие"/>
          <PrevArrow/>
        </button>
        <div className="Poster__current">
          <svg role="img" area-label={imageList[currentImage].title} preserveAspectRatio="xMidYMid slice"
               viewBox="0 0 1024 600" className="image">
            <image width="100%" height="100%" xlinkHref={imageList[currentImage].src}></image>
          </svg>
          <button className="Poster__buy-btn Button-primary" type="button">Билеты онлайн</button>
        </div>

        <button className="Poster__carousel-nav Poster__carousel-next" area-label="Следующее событие" type="button"
                onClick={handleClickNext}>
          <NextArrow/>
          <span className="visibility-hidden" area-label="Следующее событие"/>
        </button>
      </div>
      <div className="Poster__actions">
        <button className="Poster__action Button-primary" type="button">Купить билеты</button>
        <button className="Poster__action Button-primary" type="button">Стать другом</button>
        <button className="Poster__action Button-primary" type="button">Онлайн магазин</button>
      </div>
    </section>
  )
}
