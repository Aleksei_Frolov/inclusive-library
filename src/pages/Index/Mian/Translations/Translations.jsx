import React from "react";


import './index.scss'

export default function Live() {
  return (
    <section id="translations" className="Translations">
      <h2>Онлайн-трансляции</h2>
      <div className="Translations__video">
        <iframe title="Онлайн-трансляции из музея" width="100%" height="100%" src="https://www.youtube.com/embed/5qRR-5H1XgU" frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen></iframe>
      </div>
    </section>
  )
}
