import React, {useState, useEffect} from "react";

import Main from "./Mian/Main";
import Header from "./Header/Header";
import Login from "./Login/Login";

export default function IndexPage() {
  const [showLogin, setShowLogin] = useState(false);
  const handleOpenLogin = () => {
    setShowLogin(true);
    setTimeout(() => {
      document.body.classList.add('overflow-hidden');
      document.getElementById('close-button').focus();
    }, 200)
  }

  const handleCloseLogin = () => {
    setShowLogin(false);
    setTimeout(() => {
      document.body.classList.remove('overflow-hidden');
      document.getElementById('login-button').focus();
    }, 200)
  }

  useEffect(() => {
    document.onkeydown = function(evt) {
      evt = evt || window.event;
      let isEscape = false;
      if ("key" in evt) {
        isEscape = (evt.key === "Escape" || evt.key === "Esc");
      } else {
        isEscape = (evt.keyCode === 27);
      }
      if (isEscape) {
        handleCloseLogin();
      }
    };
  })

  return (
    <>
      <Header onClickLogin={handleOpenLogin} />
      <Main />
      { showLogin && (
        // <FocusTrap>
          <Login onCloseLogin={handleCloseLogin} />
        // </FocusTrap>
      )}
    </>
  )
}
