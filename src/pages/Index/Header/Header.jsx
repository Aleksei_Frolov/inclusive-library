import React, {useState} from "react";
import cn from 'classnames';
import Logo from '../../../assets/logo.svg';
import SearchIcon from '../../../assets/search.svg';
import ProfileIcon from '../../../assets/profile.svg';

import "./index.scss";

export default function Header({onClickLogin}) {
  const [query, setQuery] = useState('');
  const [lang, setLang] = useState('ru');

  const handleChangeLang = (lang) => {
    setLang(lang);
    document.querySelector('html').setAttribute('lang', lang);
  }

  const handleClickProfile = () => {
    onClickLogin(true);
  }

  const handleClickMain = () => {
    document.querySelector('.Main__menu-item').focus();
  }

  return (
    <section className="Header">
      <button className="Header__go-to-main Button-default active" type="button" aria-hidden="true" onClick={handleClickMain}>
        Перейти к главному
      </button>
      <div className="Header__icon-wrapper">
        <a href="/">
          <Logo role="img" aria-label="Инклюзивный Музей" aria-hidden="true"/>
        </a>
      </div>
      <div className="Header__naw-wrapper">
        <div className="Header__search-wrapper">
          <input type="search" className="Header__search-input" aria-label="Поиск по сайту"
                 value={query}
                 onChange={({target: {value}}) => setQuery(value)}/>
          <SearchIcon aria-hidden="true" className="Header__search-icon"/>
        </div>

        <div className="Header__lang-buttons">
          <button className={cn("Header__lang-button Button-default", {active: lang === 'en'})} onClick={() => handleChangeLang('en')}>EN
          </button>
          <button className={cn("Header__lang-button Button-default", {active: lang === 'ru'})} onClick={() => handleChangeLang('ru')}>RU
          </button>
        </div>

        <div className="Header__profile-wrapper">
          <button id="login-button" type="button" className={'Header__profile-btn'} onClick={handleClickProfile}>
            <ProfileIcon role="button" aria-label="Мой профиль"/>
          </button>
        </div>
      </div>
    </section>
  )
}
