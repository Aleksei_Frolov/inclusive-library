import React from "react";

import PostIcon from '../../assets/post-icon.svg';
import FbIcon from '../../assets/fb-icon.svg';
import TelegramIcon from '../../assets/telega-icon.svg';
import './index.scss'

export default function Login() {
  return (
    <section className="Footer">
      <div className="Footer__wrapper">
        <div className="Footer__links">
          <div>
            <a className="Footer__link" href="/#special-abilities">Посетителям с ограниченными возможностями</a>
          </div>
          <div>
            <a className="Footer__link" href="/#tickets-and-privileges">Билеты и льготы</a>
          </div>
          <div>
            <a className="Footer__link" href="/#anti-corruption">Противодействие коррупции</a>
          </div>
          <div className="Footer__social-links">
            <h3 className="Footer__title">Соц. сети</h3>
            <button className="Footer__social-btn">
              <PostIcon aria-hidden="true" aria-label="Поделиться ссылкой"/>
            </button>
            <button className="Footer__social-btn">
              <TelegramIcon aria-hidden="true" aria-label="Поделиться в Телеграм"/>
            </button>
            <button className="Footer__social-btn">
              <FbIcon aria-hidden="true" aria-label="Поделиться Фейсбук"/>
            </button>
          </div>
        </div>
        <div className="Footer__subscribe">
          <h3 className="Footer__title">Подписка на новости</h3>
          <div className="Footer__subscribe-email">
            <form>
              <div className="Footer__subscribe-input-wrapper">
                <input name="email" aria-label="Ваша эл. почта" className="Footer__subscribe-input" type="email"
                       required aria-required placeholder="E-mail"/>
              </div>

              <button className="Footer__subscribe-btn Button-primary" type="submit">Подписаться</button>
              <label className="Footer__subscribe-checkbox">
                <input type="checkbox" required aria-required/>
                Согласен на обработку персональх данных
              </label>
            </form>

          </div>
        </div>

      </div>
    </section>
  )
}
