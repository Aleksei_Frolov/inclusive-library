import "focus-visible";
import Footer from "../Footer/Footer";

import './index.scss';

const Layout = ({children}) => {
  return (
    <div className="js-focus-visible" data-js-focus-visible="">
      <div className="Layout">
        <h1 className="visibility-hidden">Инклюзивный музей</h1>
        <div className="Layout__content">
          {children}
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default Layout
